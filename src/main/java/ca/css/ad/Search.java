package ca.css.ad;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import ca.css.domain.User;

public class Search extends AbstractMessageTransformer {
	
	Log logger = LogFactory.getLog(getClass());
	
	private ActiveDirectoryHelper helper;

	public ActiveDirectoryHelper getHelper() {
		return helper;
	}


	public void setHelper(ActiveDirectoryHelper helper) {
		this.helper = helper;
	}

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		String searchStr = message.getInvocationProperty("searchStr");
		List<User> results = helper.searchUser(searchStr);
		
		for (User user : results) {
			logger.debug(user);
		}
		
		return results;
	}

}
