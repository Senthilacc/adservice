package ca.css.ad;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class Authenticate extends AbstractMessageTransformer {
	
	private ActiveDirectoryHelper helper;

	public ActiveDirectoryHelper getHelper() {
		return helper;
	}


	public void setHelper(ActiveDirectoryHelper helper) {
		this.helper = helper;
	}

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		return helper.authenticate(message.getInvocationProperty("username"), message.getInvocationProperty("password"));
	}


	
	
}
