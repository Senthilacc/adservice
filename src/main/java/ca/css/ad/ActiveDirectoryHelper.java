package ca.css.ad;

import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.filter.OrFilter;
import org.springframework.ldap.filter.WhitespaceWildcardsFilter;

import ca.css.domain.User;

public class ActiveDirectoryHelper {
	private LdapTemplate template;

	public LdapTemplate getTemplate() {
		return template;
	}

	public void setTemplate(LdapTemplate template) {
		this.template = template;
	}
	
	public List<User> searchUser(String searchStr){
		OrFilter orFilter = new OrFilter();
		AndFilter andFilter = new AndFilter();
		andFilter.and(new EqualsFilter("objectClass","user"));
		andFilter.and(orFilter.or(new WhitespaceWildcardsFilter("displayName",searchStr)).or(new EqualsFilter("sAMAccountName",searchStr))) ;
		
		
		List<User> results = template.search("CN=Users", andFilter.encode(),  new AttributesMapper<User>() {
		
	          public User mapFromAttributes(Attributes attrs)
	                  throws NamingException {
	        	  
	        	  User obj = new User();
	        	  obj.setFullName(getValue(attrs,"displayName"));
	        	  obj.setEmail(getValue(attrs,"mail"));
	        	  obj.setUserId(getValue(attrs,"sAMAccountName"));
	        	  obj.setTelephoneNumber(getValue(attrs,"telephoneNumber"));
	                  return obj;
	               }
	          	private String getValue(Attributes attrs, String key){
	          		Attribute attr = attrs.get(key);
	          		Object result = null;
	          		if(attr == null) return null;
	          		try {
	          			result = attr.get();
						if(attr.get() == null) return null;
					} catch (NamingException e) {
						return null;
					}
	          		return result.toString();
	          	}
	            });
		
		return results;
	}
	
	public User authenticate(String username, String password){
		Filter filter = new EqualsFilter("CN", username);
		boolean authed = template.authenticate("CN=Users",
	    filter.encode(),
	    password);
		
		if(authed){
			return searchUser(username).get(0);
		}
		return null;
	}

}
